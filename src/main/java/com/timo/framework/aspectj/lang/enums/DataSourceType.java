package com.timo.framework.aspectj.lang.enums;

/**
 * 数据源
 * 
 * @author timo
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
