package com.timo.framework.aspectj.lang.enums;

/**
 * 操作状态
 * 
 * @author timo
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
